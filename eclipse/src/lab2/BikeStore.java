package lab2;
//Maria Barba 1932657
public class BikeStore {
	
public static void main(String [] args) {
	Bicycle[] myBicycles = new Bicycle[4];
	myBicycles[0]=new Bicycle("Manufacturer1",23,60);
	myBicycles[1]=new Bicycle("Manufacturer2",20,61);
	myBicycles[2]=new Bicycle("Manufacturer3",25,55);
	myBicycles[3]=new Bicycle("Manufacturer4",22,43);
	printElements(myBicycles);
	}
	private static void printElements(Bicycle[] myBicycles) {
		for(int i=0;i<4;i++) {
		System.out.println(myBicycles[i].toString());
		}
}
}
